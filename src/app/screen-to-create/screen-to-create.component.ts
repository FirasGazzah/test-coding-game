import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-screen-to-create',
  templateUrl: './screen-to-create.component.html',
  styleUrls: ['./screen-to-create.component.scss'],
  providers: [DatePipe]
})
export class ScreenToCreateComponent implements OnInit {
  formattedDate: any
  date = Date()
  initProgress = 50
  devProgress = 25
  constructor(private datePipe: DatePipe) {
    this.formattedDate = 't ' + this.datePipe.transform(this.date, 'dd/MM/yyyy');
  }

  ngOnInit(): void {
  }
  addProgress(value: any){
    if(this.initProgress + value <= 100){
      this.initProgress = this.initProgress + value
    }
    if(this.devProgress + value <= 100){
      this.devProgress = this.devProgress + value
    }
  }

  reset(){
    this.devProgress = 0
    this.initProgress = 0
  }

}
