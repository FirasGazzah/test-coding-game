import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ScreenToCreateComponent } from './screen-to-create.component';

describe('ScreenToCreateComponent', () => {
  let component: ScreenToCreateComponent;
  let fixture: ComponentFixture<ScreenToCreateComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ScreenToCreateComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ScreenToCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
