import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-progress-bar',
  templateUrl: './progress-bar.component.html',
  styleUrls: ['./progress-bar.component.scss'],
})
export class ProgressBarComponent implements OnInit {
  @Input() title: any
  @Input() progressValue = 50
  color = 'rgb(113, 96, 232)';

  @Input() progress: any

  constructor() { }

  ngOnInit(): void {
  }

}
